<?php
/*
Plugin Name: Ad Zone DFP v3
Description: Select an ad based on category ( on GitLab )
Author: Jeff Messeroll
License: Public Domain
Version: 3.0.2
*/

 /* ---------------------------------- *
 * constants
 * ---------------------------------- */
include( plugin_dir_path( __FILE__ ) . 'settings.php' );

 /* ---------------------------------- *
 * enque scripts
 * ---------------------------------- */
function jam_adzone_style(){
wp_enqueue_style( 'form5-style', plugins_url( 'style.css', __FILE__ ));
}
add_action('admin_enqueue_scripts', 'jam_adzone_style');

add_action('wp_footer', 'jam_dfp_footer');

function jam_dfp_footer(){
    echo "<!--- Ending SRA DFP -->
    <script>
        googletag.cmd.push(function() {
          // This call to display requests both ad slots with all
          // configured targeting.
          googletag.display(adSlot4);
        });
      </script>";
}

 
 /* ----------------------------------
  * Switched to using a single call to DFP
  ------------------------------------ */

add_action( 'wp_head', 'jam_dfp_header' );

function jam_dfp_header(){

global $wpdb;
    
    
    // get host name
    $urlparts = parse_url(home_url());
    $host = $urlparts['host'];
    $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    $uri = $uriSegments[1];
    //echo $uri;
 

      
    // get options
    $options = get_option('jam_adzone');
    $slot = $options['slot'];

    // get category or host name if a page
    $categories = get_the_category();
    if ( empty($categories) ) {
      $cat_name = single_cat_title( $prefix = '', $display = true );
    }else{
      $cat_id = $categories[0]->cat_ID;
      $cat_name = get_the_category_by_ID( $cat_id );
    }

    if ( get_query_var('cat') ) { // a category page 
      $cat_name = single_cat_title( $prefix = '', $display = false );
    } // end category page

    // check for special pages
    if ( $uri == 'snjtoday' ) {
      $cat_name = "SNJ Today" ;
    }

    // IF we are not able to grab a category use the hostname
    if ( empty($cat_name)){
      $cat_name = str_replace ( '.','-',$host);
    }
    
    // If a single post then we need a 300x250 island ad
    if ( is_single( ) ) {
        // The current single page
        $single = 1 ;
       } else {
        // Perform default actions
        $single = 0 ;
       }


// build DFP header script
?>
<!-- Google DFP header -->
<?php // return DFP tag
	$dfpheader = '';
	$dfpheader = '<meta property="wp:content" content="' . $cat_name . '">';
	$dfpheader .= "
<script async src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>

<script>
      window.googletag = window.googletag || {cmd: []};
      var adSlot1, adSlot2, adSlot3, adSlot4;

    googletag.cmd.push(function() {
     // Define a size mapping object. The first parameter to addSize is
     // a viewport size, while the second is a list of allowed ad sizes.
     var mapping = googletag.sizeMapping().

     // Accepts both common mobile banner formats
     addSize([320, 400], [320, 50]). 
                
     // Landscape tablet 
     addSize([750, 200], [728, 90]). 

     // Desktop
     addSize([1050, 200], [728, 90]).build();

        // Define ad slot 1.
        adSlot1 = googletag
            .defineSlot('" . $slot . "', [320, 50], 'adSlot1')
            .setTargeting('position', 'header')
            .defineSizeMapping(mapping)
            .addService(googletag.pubads());
        // Define and configure ad slot 2.
        adSlot2 = googletag
            .defineSlot('" . $slot . "', [300, 600], 'adSlot2')
            .setTargeting('position', 'sidebar')
            .addService(googletag.pubads());";
    
    if ( $single == 1 ) {
        $dfpheader .= "
        // Define and configure ad slot 3.
	    adSlot3 = googletag
            .defineSlot('" . $slot . "', [300, 250], 'adSlot3')
            .setTargeting('position', 'inline')
            .addService(googletag.pubads());";
    }

	$dfpheader .= "
        // Define and configure ad slot 4.
        adSlot4 = googletag
            .defineSlot('" . $slot . "', [320, 50], 'adSlot4')
            .setTargeting('position', 'footer')
            .defineSizeMapping(mapping)
            .addService(googletag.pubads());

        // Enable SRA and services.
        googletag.pubads().setTargeting('town', '" . $cat_name . "');
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>";

	echo $dfpheader;

} //end dfp header
