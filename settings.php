<?php
/*
	admin settings page
*/

add_action('admin_init', 'jam_adzone_init' );
add_action('admin_menu', 'jam_adzone_add_page');

// Init plugin options to white list our options
function jam_adzone_init(){
	register_setting( 'jam_adzone_options', 'jam_adzone', 'jam_adzone_validate' );
}

// Add menu page
function jam_adzone_add_page() {
	add_options_page('Ad Manager', 'Ad Manager', 'manage_options', 'jam_adzone', 'jam_adzone_do_page');
}

// Draw the menu page itself
function jam_adzone_do_page() {
	?>

	<div class="wrap">
		<h2>Google Ad Manager Options</h2>
		<div class="form-style-5">
		<form method="post" action="options.php">
		    <?php settings_fields('jam_adzone_options'); ?>
			<?php $options = get_option('jam_adzone'); ?>
			

<legend><span class="number">0</span> Basic setup Info</legend>

<span style="width: 250px; float: left; margin: 10px">
<label for="jam_adzone[slot]">Name of Town array</label>
<input type="text" name="jam_adzone[townarray]" value="<?php echo $options['townarray']; ?>" placeholder=" " >
</span>

<span style="width: 250px; float: left; margin: 10px">
<label for="jam_adzone[slot]">Path from Google Ad Manager</label>
<input type="text" name="jam_adzone[slot]" value="<?php echo $options['slot']; ?>" placeholder=" " >
</span>

<input type="submit" value="Apply" />

		</form>
	</div>
	<?php	
}

// Sanitize and validate input. Accepts an array, return a sanitized array.
function jam_adzone_validate($input) {
	
	return $input;
}
